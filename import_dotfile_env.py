# Sourcing a shell script that sets environment variables to use in the script
# Source: http://pythonwise.blogspot.com/2010/04/sourcing-shell-script.html

import os
from subprocess import Popen, PIPE

env_filename = "test.env"

# create environment file in working dir (just so it does not clutter my snippets-git)
with open(env_filename, 'w') as f:
    f.write("""## env.prc
export TESTVAR=this_is_a_test
""")

# get the absolute (!) path to the environment file
working_dir = os.path.abspath(os.path.dirname(__file__))
# the environment file (in this case) must be in the working dirs
env_path = os.path.join(working_dir, env_filename)

def source(script):
    """Returns the environment variables as a dict"""
    # run the environment-script and return the changed environment
    pipe = Popen(". {}; env -0".format(script), stdout=PIPE, shell=True)
    # get the bytestring returned by the pipe-operator
    data = pipe.communicate()[0]
    # decode the bytestring
    data = data.decode()
    data_str = data.split('\0')
    # put all variables with assigned values into a dict
    env = dict((line.split("=", 1) for line in data_str if "=" in line))
    return env

# get the environment variables
env = source(env_path)

# set the environment variables
os.environ.update(env)

#get the variable
# BEWARE: this works only if the variable has been exported (which is how
# environment variables work)
TESTVAR = os.getenv("TESTVAR")

print(TESTVAR)
